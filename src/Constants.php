<?php
const IMPORT_DIRECTORY = __DIR__ . '/../imports/';
const EXPORT_DIRECTORY = __DIR__ . '/../exports/';

const FINANCE_CHECK_LIST = [1, 2, 3, 4, 5, 6, 7, 8];
const FINANCE_CHECK_THRESHOLD = 0.05;

const FINANCIAL_YEAR_START_DATE = '2021-10-01';

const KNOWN_BOOKINGS_FILE = '../known.csv';
const ERROR_CHECK_FILE = '../error_check.csv';

const THIRD_PARTY_AGENTS = [
    22909 => 'Airbnb Commission',
    23796 => 'Airbnb uplift',
    17792 => 'Airbnb',
    24064 => 'HomeAway uplift',
    24070 => 'HomeAway uplift',
    24073 => 'HomeAway uplift',
    24100 => 'HomeAway uplift',
    23865 => 'HomeAway uplift',
    23866 => 'HomeAway uplift',
    24122 => 'HomeAway uplift',
    23869 => 'HomeAway uplift',
    23870 => 'HomeAway uplift',
    23873 => 'HomeAway uplift',
    24129 => 'HomeAway uplift',
    24134 => 'HomeAway uplift',
    23891 => 'HomeAway uplift',
    23904 => 'HomeAway uplift',
    24162 => 'HomeAway uplift',
    23924 => 'HomeAway uplift',
    24203 => 'HomeAway uplift',
    24204 => 'HomeAway uplift',
    23950 => 'HomeAway uplift',
    24212 => 'HomeAway uplift',
    24218 => 'HomeAway uplift',
    23978 => 'HomeAway uplift',
    23979 => 'HomeAway uplift',
    23999 => 'HomeAway uplift',
    24002 => 'HomeAway uplift',
    24005 => 'HomeAway uplift',
    24006 => 'HomeAway uplift',
    24028 => 'HomeAway uplift',
    24038 => 'HomeAway uplift',
    22652 => 'TripAdvisor Commission',
    26774 => 'TripAdvisor uplift',
];

const HEADERS_FILTERED_BOOKINGS = [
    'Booking Ref',
    'Total Cost',
    'Total Rent',
    'Booking Fee',
    'Extras',
    'Due to owner RENT',
    'Due to owner EXTRAS',
    'Total Due to owner',
    'Paid to owner',
    'OWNER LIABILITY',
    'Total to CCH',
    'Comm Gross',
    'Comm Net',
    'Comm VAT',
    'Extras Comm Gross',
    'Extras Comm Net',
    'Extras Comm VAT',
    'Total Received From Customer',
    'Total Relating to Holiday',
    'Fake Cash',
    'Third Party Comm Supercontrol',
    'Third Party Comm Calculated',
    'Third Party Commission Percentage',
    'Pure Credit Booking',
    'Balance Due on Holiday',
    'SD Amount',
    'SD Paid',
    'SD Refunded',
    'Balance of SD Held',
    'Status',
    'Property ref',
    'Owner ref',
    'Holiday booked date',
    'Holiday start date',
    'Holiday end date',
    'Source',
    'Comm%',
    'Property Comm%',
    'Property Name',
    'Property Unique Ref',
    'Discount',
    'Voucher',
    'Amount Due to Owner (today)',
    'Amount Due to CCH (today)',
    'Cancelled Date',
    'Cancellation Rate',
    'Booking Type',
    'Channel Fee',
    'Reclaimed From Owner',
    'Paid Per Statement',
    'Compensation/Reversal/OwnerCancellationFee',
    'Cash Received Allocated to Agency First',
    'Balance of Cash After Allocation to Agency',
    'Total due to the Home Owner',
    'Balance Remaining to Home Owner After Payments to Date',
    'Debtors Control Account',
    'Debtors2',
    'Bank Current Account',
    'Sykes Income',
    'Sykes Deferred',
    'Owner Deposit',
    'Owner Deferred',
];

const FINANCE_CHECK_HEADERS = [
    'Check 1',
    'Check 2',
    'Check 3',
    'Check 4',
    'Check 5',
    'Check 6',
    'Check 7',
    'Check 8',
    'Check 9',
    'Check 10',
    'Check 11',
    'Check 12',
    'Check 13',
    'Check 14',
    'Check 15',
    'Check 16',
];

const FINANCE_CHECK_HEADER_DESCRIPTIONS = [
    'Check 1'  => 'Total Cost - Total Rent - Booking Fee - Extras',
    'Check 2'  => 'Total Cost - Total Due to owner - Total to CCH',
    'Check 3'  => 'Total Due to owner - Due to owner EXTRAS - Due to owner RENT',
    'Check 4'  => 'Total to CCH - Comm Gross - Booking Fee - Extras Comm Gross',
    'Check 5'  => 'Total Rent - Due to owner RENT - (Comm Gross)',
    'Check 6'  => 'Extras - Due to owner EXTRAS - Extras Comm Gross',
    'Check 7'  => 'Total Due to owner - Paid to owner - OWNER LIABILITY',
    'Check 8'  => 'Total Cost - Total Relating to Holiday - Balance Due on Holiday',
    'Check 9'  => 'Property Comm% - Comm%',
    'Check 10' => 'Paid to owner - Paid Per Statement',
    'Check 11' => 'Debtors Control Account - Bank Current Account - Sykes Income - Sykes Deferred - Owner Deposit - Owner Deferred',
    'Check 12' => 'Total Cost - Debtors Control Account - Bank Current Account',
    'Check 13' => 'Total Due to owner + Owner Deposit + Owner Deferred',
    'Check 14' => 'Balance Due on Holiday - Debtors Control Account',
    'Check 15' => 'Total Relating to Holiday + Sykes Income + Owner Deposit',
    'Check 16' => 'Debtors Control Account + Sykes Deferred + Owner Deferred',
];

const DODGY_BOOKINGS = [
    //Bookings failing Test 1

    //Bookings failing Test 2
    1032227, // Trip Advisor
    1018680, // Trip Advisor
    1019586, // Trip Advisor sum more than total cost
    1032518, // Trip Advisor sum more than total cost
    1031271, // Trip Advisor sum more than total cost
    1032541, // Trip Advisor sum more than total cost
    1033191, // Trip Advisor sum more than total cost

    //Bookings failing Test 3

    //Bookings failing Test 4

    //Bookings failing Test 5
    1032337, // sum less than total cost
    //Bookings failing Test 6

    //Bookings failing Test 7

    //Bookings failing Test 8
];