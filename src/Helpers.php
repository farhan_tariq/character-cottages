<?php
include_once 'dotenv.php';
(new DotEnv(__DIR__ . '/../.env'))->load();

class Helpers
{
    public static function array_stripos($string, $array): bool
    {
        $array = (array)$array;
        foreach ($array as $s) {
            $pos = stripos($string, $s);
            if ($pos !== false) {
                return true;
            }
        }
        return false;
    }

    public static function roundFloat($num): float
    {
        return round($num, Constants::DECIMAL_PRECISION);
    }

    public static function debugBooking(object $booking, int $bookingId, array $fields, bool $die = false): void
    {
        if ((int)$booking->__pk === $bookingId) {
            print_r($fields);

            if ($die) {
                die('Finished debugBooking()');
            }
        }
    }

    public static function stringArrayToLowerCase($array): array
    {
        $data = [];
        foreach ($array as $key => $item) {
            $data[$key] = strtolower($item);
        }

        return $data;
    }

   public static function initDB()
    {
        $servername = getenv('DB_SERVERNAME');
        $username   = getenv('DB_USERNAME');
        $password   = getenv('DB_PASSWORD');
        $database   = getenv('DB_NAME');

        // Create connection
        $conn = mysqli_connect($servername, $username, $password, $database);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        echo 'Connected' . PHP_EOL;
        return $conn;
    }

    public static function closeDB($link): void
    {
        mysqli_close($link);
    }
}