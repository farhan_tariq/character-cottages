<?php
include_once 'Helpers.php';
include_once 'FinanceChecks.php';
include_once 'Constants.php';

$conn                    = Helpers::initDB();
$financeChecks           = new FinanceChecks();
$bookings                = getAllBookings($conn);
$customerPayments        = getCustomerPayments($conn, $bookings);
$thirdPartyCommissions   = getThirdPartyAgentCommission($conn, $bookings);
$bookingsExtras          = getAllExtras($conn, $bookings);
$ownerPayments           = getAllOwnerPayments($conn, $bookings);
$ownerPaymentAdjustments = getManualOwnerAdjustments($conn, $bookings);
$securityDeposits        = getAllSecurityDeposits($conn, $bookings);
$outputFileName          = 'cch-filtered-bookings.14thDec-Go-Live-dump-Cancelled.' . date('Y-m-d--H-i-s') . '.csv';
$out                     = null;
$onlyConfirmedBookings   = true;

if (empty($argv[1])) {
    $out = fopen(EXPORT_DIRECTORY . $outputFileName, 'wb');

    if (!$out) {
        die('Could not open file: "' . EXPORT_DIRECTORY . $outputFileName . '"');
    }

    $descriptions = [];
    for ($i = 0; $i < 57; $i++) {
        $descriptions[] = '';
    }
    fputcsv($out, array_merge($descriptions, FINANCE_CHECK_HEADER_DESCRIPTIONS));
    fputcsv($out, array_merge(HEADERS_FILTERED_BOOKINGS, FINANCE_CHECK_HEADERS));
}

$knownFile = fopen(IMPORT_DIRECTORY . KNOWN_BOOKINGS_FILE, 'rb');

if (!$knownFile) {
    die('Could not open file: "' . IMPORT_DIRECTORY . KNOWN_BOOKINGS_FILE . '"');
}

$headers = fgetcsv($knownFile);
$known = [];

while (($row = fgetcsv($knownFile)) !== false) {
    $row = array_combine($headers, $row);
    $known[$row['Booking Ref']] = $row;
}

foreach ($bookings as $booking) {
    if (filterBooking($booking, $onlyConfirmedBookings) ) {
        $sdPaid     = $booking->security_deposit_paid_amount;
        $sdRefunded = getSecurityDepositRefunded($booking);
        $sdHeld     = $sdPaid > 0 && $sdRefunded == 0 ? $sdPaid : round($sdPaid - $sdRefunded, 2);

        $totalReceivedFromCustomer = round($booking->total_received_from_customer - $booking->total_refunded_to_customer, 2);
        $totalRelatingToHoliday    = round($totalReceivedFromCustomer, 2);

        $rentalPrice = getRentalPrice($booking);
        $ownerExtras = getOwnerExtrasTotal($booking);
        $extrasTotal = getExtrasCommGross($booking);
        $channelFee  = getBookingChannelFee($booking);

        $booking->cch_extras_total   = $extrasTotal;
        $booking->owner_extras_total = $ownerExtras;
        $booking->channel_fee        = $channelFee;

        $totalPrice           = $rentalPrice + $extrasTotal + $ownerExtras + $channelFee;
        $reclaimedFromOwner   = getSecurityDepositFeeClaimedFromOwner($booking) * -1;
        $totalDueToOwner      = $booking->due_to_owner + $reclaimedFromOwner;
        $ownerLiability       = round($totalDueToOwner - $booking->paid_to_owner, 2);
        $fakeCash             = getFakeCash($booking);
        $pureCreditBooking    = !empty($booking->third_party_commission) || $fakeCash > 0 ? 1 : 0;
        $thirdPartyPercentage = 0;

        if ($pureCreditBooking && $booking->source === 'Airbnb') {
            if ($booking->booked_date < '2021-11-26') {
                $thirdPartyPercentage = .18;
            }

            if ($booking->booked_date >= '2021-11-26') {
                $thirdPartyPercentage = .15;
            }

            if ($booking->booked_date < '2021-01-01') {
                $thirdPartyPercentage = .168;
            }
        }

        $calculatedThirdParty = $totalPrice * $thirdPartyPercentage;

        if ($totalReceivedFromCustomer > $totalPrice) {
            $totalRelatingToHoliday -= $sdHeld;
        }

        if ($totalRelatingToHoliday == 0 && $pureCreditBooking && $booking->source === 'Airbnb' && $booking->from_date <= '2022-01-25') {
            $totalRelatingToHoliday =  $totalPrice * (1 - $thirdPartyPercentage);
        }

        $balanceDueOnHoliday            = round($totalPrice - $totalRelatingToHoliday, 2);
        $amountDueToOwnerToday          = getAmountDueToOwnerToday($booking, $totalRelatingToHoliday);
        $totalToCCH                     = getTotalToCCH($booking);
        $calculatedCommissionPercentage = (getPropertyCommissionPercentage($booking) * 100) / 1.2;

        $cashReceivedAllocatedToAgencyFirst         = $channelFee + (($totalRelatingToHoliday - $channelFee) * (($calculatedCommissionPercentage / 100) * 1.2));
        $balanceOfCashAfterAllocationLeftForOwner   = $totalRelatingToHoliday - $cashReceivedAllocatedToAgencyFirst;
        $balanceRemainingToOwnerAfterPaymentsToDate = $balanceOfCashAfterAllocationLeftForOwner - $totalDueToOwner;

        $debtorsControlAccount =  $balanceDueOnHoliday;
        $debtors2              = 0;
        $bankCurrentAccount    = $totalRelatingToHoliday;
        $ownerDeposits         = round($balanceOfCashAfterAllocationLeftForOwner * -1 , 2);
        $sykesIncome           = round(($bankCurrentAccount * -1) - $ownerDeposits, 2);
        $ownerDeferred         = round(($totalDueToOwner * -1) - $ownerDeposits, 2);
        $sykesDeferred         = round(($totalToCCH * -1) - $sykesIncome, 2);

        $row = [
            $booking->id,                               // 'Booking Ref'
            $totalPrice,                                // 'Total Cost'
            $rentalPrice,                               // 'Total Rent'
            $channelFee,                                // 'Booking Fee'
            $extrasTotal+ $ownerExtras,                 // 'Extras'
            $totalDueToOwner - $ownerExtras,            // 'Due to owner RENT'
            $ownerExtras,                               // 'Due to owner EXTRAS'
            $totalDueToOwner,                           // 'Total Due to owner'
            $booking->paid_to_owner,                    // 'Paid to owner'
            $ownerLiability,                            // 'OWNER LIABILITY'
            $totalToCCH,                                // 'Total to CCH'
            getCommissionGross($booking),               // 'Comm Gross'
            getCommissionNet($booking),                 // 'Comm Net'
            getCommissionVat($booking),                 // 'Comm VAT'
            $extrasTotal,                               // 'Extras Comm Gross'
            getExtrasCommNet($booking),                 // 'Extras Comm Net'
            getExtrasCommVAT($booking),                 // 'Extras Comm VAT'
            $totalReceivedFromCustomer,
            $totalRelatingToHoliday,
            $fakeCash,
            $booking->third_party_commission_amount,
            $calculatedThirdParty,
            $totalPrice > 0 ?($fakeCash + $booking->third_party_commission_amount) / $totalPrice : 0,
            $pureCreditBooking,
            $balanceDueOnHoliday,
            $booking->security_deposit_amount,
            $sdPaid,
            $sdRefunded,
            $sdHeld,
            $booking->status,
            $booking->_fk_property,
            $booking->_fk_owner,
            $booking->booked_date,
            $booking->from_date,
            $booking->to_date,
            $booking->source,
            $calculatedCommissionPercentage,
            $booking->property_commission_percentage,
            $booking->property_name,
            $booking->property_unique_ref,
            $booking->discount,
            $booking->voucher,
            $booking->due_to_owner,
            'Amount Due to CCH (today)',
            $booking->cancelled_date,
            $booking->cancelled_bookingfullrate,
            $booking->booking_type,
            $channelFee,
            $reclaimedFromOwner,
            getAmountPaidPerStatement($booking),
            $booking->owner_payments_adjusted_amount,
            $cashReceivedAllocatedToAgencyFirst,
            $balanceOfCashAfterAllocationLeftForOwner,
            $totalDueToOwner,
            $balanceRemainingToOwnerAfterPaymentsToDate,
            $debtorsControlAccount,
            $debtors2,
            $bankCurrentAccount,
            $sykesIncome,
            $sykesDeferred,
            $ownerDeposits,
            $ownerDeferred,
        ];

        $row = array_combine(HEADERS_FILTERED_BOOKINGS, $row);

        $financeChecks->printBooking($argv, $booking, $row);
        $financeChecks->runFinanceChecks($argv, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
        $financeChecks->sumFinanceChecks($row);
        $financeChecks->checkBooking($known, $booking, $row);

        $financeChecksResults = $financeChecks->getBookingsErrorCheck()[$booking->id];
        $financeChecksRow = array_combine(FINANCE_CHECK_HEADERS, (array)$financeChecksResults);
        $row = array_merge($row, $financeChecksRow);

        if (empty($argv[1])) {
            fputcsv($out, $row);
        }
    }
}

if (empty($argv[1])) {
    fclose($out);
    echo 'Saved file: ' . $outputFileName . PHP_EOL . PHP_EOL;
    $financeChecks->compareFinanceChecks();
    $financeChecks->appendDataToErrorCheck(ERROR_CHECK_FILE);
}

//$financeChecks->outputErrorReport('check8');

function getAmountDueToOwnerToday($booking, $totalRelatingToHoliday): string
{
    if (
        $booking->due_to_owner > 0 &&
        $booking->paid_to_owner == 0 &&
        (($totalRelatingToHoliday > 0 && $totalRelatingToHoliday <= $booking->total_price) ||
            ($totalRelatingToHoliday > 0 && $totalRelatingToHoliday >= $booking->total_price))
    ) {
        return 'getAmountDueToOwnerToday';
    }

    return '';
}

function getRentalPrice($booking)
{
    $rental = $booking->adjusted_rent;

    if (strtolower($booking->status) !== 'confirmed') {
        if ($booking->adjusted_rent > 0){
            $rental = $booking->adjusted_rent;
        }else{
            $rental = $booking->cancelled_bookingfullrate;
        }
    }

    $booking->calculated_rental = $rental - getExtrasIncludedInRentalPrice($booking);

    return $booking->calculated_rental;

}

function getTotalToCCH($booking): float
{
    return round((getBookingCommission($booking) + $booking->channel_fee), 2);
}

function getCommissionNet($booking): float
{
    return round((getBookingCommission($booking) - getExtrasCommGross($booking)) / 1.2, 2);
}

function getCommissionVAT($booking): float
{
    return round(getCommissionNet($booking) * 0.2, 2);
}

function getCommissionGross($booking): float
{
    return getCommissionNet($booking) + getCommissionVat($booking);
}

function getExtrasCommNet($booking): float
{
    $total = 0;
    $commissionPercentage = getPropertyCommissionPercentage($booking) / 1.2;
    foreach ($booking->extras as $extra) {
        if ((int) $extra->commissionable === 0 && $extra->extra_type != 95272) {
            $total += $extra->total_price;
        }
    }

    return round(($total * $commissionPercentage ) / 1.2, 2);
}

function getExtrasCommVAT($booking): float
{
    $extrasCommNet = getExtrasCommNet($booking);

    return round($extrasCommNet * 1.2 - $extrasCommNet, 2);
}

function getExtrasCommGross($booking): float
{
    return round(getExtrasCommNet($booking) + getExtrasCommVAT($booking), 2);
}

function getOwnerExtrasTotal($booking): float
{
    $total = 0;
    foreach ($booking->extras as $extra) {
        if ((int) $extra->commissionable === 0 && $extra->extra_type != 95272) {
            $total += $extra->total_price;
        }
    }

    $total /= 1.2;

    return round(($total - getExtrasCommNet($booking)) * 1.2, 2);
}

function getSecurityDepositFeeClaimedFromOwner($booking): float
{
    $total = 0;
    foreach ($booking->owner_payments as $owner_payment) {
        if (strripos('- ' . $owner_payment->payment_caption, 'securi')) {
            $total += $owner_payment->amount;
        }
    }
    return $total;
}

function getSecurityDepositRefunded($booking): float
{
    $refundAmount = 0;
    $sdPayment = null;
    if ($booking->security_deposit_refunded == 0 && $booking->security_deposit_amount > 0) {
        foreach ($booking->payments as $payment) {
            if (Helpers::array_stripos($payment->paymentcaption, ['securi', 'sd']) !== false) {
                if ($payment->amount > 0) {
                    $sdPayment = $payment;
                    continue;
                }
            }

            if ($sdPayment !== null) {
                if (
                    $payment->amount < 0 &&
                    $payment->date >= $sdPayment->date &&
                    $sdPayment->amount == abs($payment->amount)
                ) {
                    $refundAmount += $sdPayment->amount;
                }
            }
        }
    }

    return $refundAmount + $booking->security_deposit_refunded;
}

function filterBooking($booking, $onlyConfirmedBookings): bool
{
    if (!$onlyConfirmedBookings){
        return $booking->to_date >= '2021-10-01';
    }

    return $booking->to_date >= '2021-10-01' && strtolower($booking->status) === 'cancelled';
}

function getPropertyCommissionPercentage($booking): float
{
    $propertyCommissionPercentage = (float)$booking->property_commission_percentage;
    $rentalPrice = $booking->calculated_rental + getExtrasIncludedInRentalPrice($booking);

    $bookingCommissionWithVAT = getBookingCommission($booking);

    if ($rentalPrice > 0) {
        if ($bookingCommissionWithVAT / $rentalPrice === $propertyCommissionPercentage) {
            return $propertyCommissionPercentage / 100;
        }

        return ($bookingCommissionWithVAT / $rentalPrice) ;
    }

    return $propertyCommissionPercentage / 100;
}

function getBookingCommission($booking): float
{
    $total = 0;
    foreach ($booking->owner_payments as $owner_payment) {
        $total += $owner_payment->commission_amount;
    }

    return $total;
}

function getAllBookings($conn): array
{
    $bookings = [];
    $sql = "
            SELECT `b`.`booking_id` AS id,
                   `b`.`_fk_property`,
                   `b`.`_fk_customer`,
                   `b`.`booked_date`,
                   `b`.`from_date`,
                   `b`.`to_date`,
                   `b`.`status`,
                   `b`.source,
                   `b`.`booking_type`,
                   `b`.`total_price`,
                   `bd`.bookingfullrate AS actual_rent,
                   `b`.`rental_price` AS adjusted_rent,
                   `bd`.`bookingdiscount_couples` + `bd`.bookingdiscount_early + `bd`.bookingdiscount_late + `bd`.bookingdiscount_multi + `bd`.bookingdiscount_oap + `bd`.bookingdiscount_spare + `bd`.bookingdiscount_xy + `bd`.bookingfixeddiscount + `bd`.bookingotherdiscount AS discount,
                   `b`.`voucher`,
                   `b`.`booking_fee_price` AS booking_fee,
                   `b`.`credit_card_fee_amount`,
                   `b`.`extras_price`,
                   `b`.`extras_commission`, /*  Always the same amount for all bookings*/
                   `b`.`commission_rate`,
                   `b`.`due_to_owner`,
                   `b`.`paid_to_owner`,
                   `b`.`outstanding_to_owner`,
                   `b`.`security_deposit_amount`,
                   `b`.`security_deposit_paid`,
                   `b`.`security_deposit_due_date`,
                   `b`.`cancelled_date`,
                   `b`.`channel_fee_value`,
                   `b`.`channel_fee_vat`,
                   `bd`.cancelled_bookingfullrate,
                   `p`.commission_percentage AS property_commission_percentage,
                   `p`.name AS property_name,
                   `p`.rcvoldID AS property_unique_ref,
                   `p`._fk_owner
            FROM `booking` b
                     JOIN `booking_details` `bd` ON `b`.__pk = bd.bookingID
                     JOIN property p ON p.__pk = b._fk_property 
            WHERE b.to_date >= '" . FINANCIAL_YEAR_START_DATE . "' AND b._fk_property NOT IN (569930,545081,551573,487318,584020,540217,487426,573240,568437,487099,487107,540404,576515,585234,587312);";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            $bookings[$obj->id] = $obj;
            $bookings[$obj->id]->third_party_commission_amount = 0;
            $bookings[$obj->id]->third_party_commission_with_vat = 0;
            $bookings[$obj->id]->total_received_from_customer = 0;
            $bookings[$obj->id]->total_refunded_to_customer = 0;
            $bookings[$obj->id]->security_deposit_paid_amount = 0;
            $bookings[$obj->id]->security_deposit_refunded = 0;
            $bookings[$obj->id]->owner_payments_adjusted_amount = 0;
            $bookings[$obj->id]->payments = [];
            $bookings[$obj->id]->third_party_commission = [];
            $bookings[$obj->id]->extras = [];
            $bookings[$obj->id]->finances = [];
            $bookings[$obj->id]->owner_payments = [];
            $bookings[$obj->id]->owner_payment_adjustments = [];
        }
    }
    echo "Fetched All Bookings" . PHP_EOL;
    return $bookings;
}

function getCustomerPayments($conn, $bookings): array
{
    $payments = [];
    $sql = "
            SELECT bp.*
            FROM booking b
                JOIN booking_payment bp ON bp.`_fk_booking` = b.`booking_id`
            WHERE b.`to_date` >= '" . FINANCIAL_YEAR_START_DATE . "';";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {

            if (!empty($bookings[$obj->_fk_booking])) {
                if ($obj->amount > 0) {
                    if (Helpers::array_stripos($obj->paymentcaption, ['securi', 'sd']) !== false) {
                        $bookings[$obj->_fk_booking]->security_deposit_paid_amount = $obj->amount;
                    } else {
                        $bookings[$obj->_fk_booking]->total_received_from_customer += $obj->amount;
                    }
                }

                if ($obj->amount < 0) {
                    if (Helpers::array_stripos($obj->paymentcaption, ['securi', 'sd']) !== false) {
                        $bookings[$obj->_fk_booking]->security_deposit_refunded = $obj->amount * -1;
                    } else {
                        $bookings[$obj->_fk_booking]->total_refunded_to_customer += $obj->amount * -1;
                    }
                }

                $bookings[$obj->_fk_booking]->payments[] = $obj;
            }
            $payments[$obj->_fk_booking][] = $obj;
        }
    }
    echo "Fetched All Payments" . PHP_EOL;
    return $payments;
}

function getThirdPartyAgentCommission($conn, $bookings): array
{
    $thirdPartyCommission = [];
    $sql = "
            SELECT bpa.* FROM booking b 
                JOIN booking_payments_agents bpa ON bpa.`owner_bookingID` = b.`booking_id`
            WHERE b.`to_date` >= '" . FINANCIAL_YEAR_START_DATE . "';";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (!empty($bookings[$obj->owner_bookingID])) {
                if ($obj->commission > 0 && ! in_array((int) $obj->agentID, [23796, 23865])) {
                    $bookings[$obj->owner_bookingID]->third_party_commission_amount += $obj->commission;
                    $bookings[$obj->owner_bookingID]->third_party_commission_with_vat += $obj->commission + $obj->commission * ($obj->commission_vat / 100);
                }

                if ($obj->commission < 0 && ! in_array((int) $obj->agentID, [23796, 23865])) {
                    $bookings[$obj->owner_bookingID]->third_party_commission_amount += $obj->commission;
                    $bookings[$obj->owner_bookingID]->third_party_commission_with_vat += $obj->commission - $obj->commission * ($obj->commission_vat / 100);
                }
                $obj->agent_name = THIRD_PARTY_AGENTS[$obj->agentID];
                $bookings[$obj->owner_bookingID]->third_party_commission[] = $obj;
            }
            $thirdPartyCommission[$obj->owner_bookingID][] = $obj;
        }
    }
    echo "Fetched All Third Party Commissions" . PHP_EOL;
    return $thirdPartyCommission;
}

function getAllExtras($conn, $bookings): array
{
    $extras = [];
    $sql = "
            SELECT b.booking_id, bi.* FROM booking_item bi 
                JOIN booking b on bi._fk_booking_detail_id = b.booking_detail_id
            WHERE b.`to_date` >= '" . FINANCIAL_YEAR_START_DATE . "';";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (!empty($bookings[$obj->booking_id])) {
                $bookings[$obj->booking_id]->extras[] = $obj;
            }
            $extras[$obj->booking_id][] = $obj;
        }
    }
    echo "Fetched All Bookings Extras" . PHP_EOL;
    return $extras;
}

function getAllBookingFinances($conn, $bookings): array
{
    $finances = [];
    $sql = "
           SELECT * FROM booking_finances bf 
            JOIN booking b ON b.booking_id = bf.owner_bookingID
           WHERE b.to_date >= '" . FINANCIAL_YEAR_START_DATE ."' AND b._fk_property NOT IN (569930,545081,551573,487318,584020,540217,487426,573240,568437,487099,487107,540404,576515,585234,587312);";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (!empty($bookings[$obj->booking_id])) {
                $bookings[$obj->booking_id]->finances[] = $obj;
            }
            $finances[$obj->booking_id][] = $obj;
        }
    }
    echo "Fetched All Bookings Finances" . PHP_EOL;
    return $finances;
}

function getAllOwnerPayments($conn, $bookings): array
{
    $ownerPayments = [];
    $sql = "
           SELECT b.booking_id AS id, opo.* FROM owner_payment_owe opo 
            JOIN booking b ON b.__pk = opo.booking_id
           WHERE b.to_date >= '" . FINANCIAL_YEAR_START_DATE . "' AND b._fk_property NOT IN (569930,545081,551573,487318,584020,540217,487426,573240,568437,487099,487107,540404,576515,585234,587312);";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (!empty($bookings[$obj->id])) {
                $bookings[$obj->id]->owner_payments[] = $obj;
            }
            $ownerPayments[$obj->id][] = $obj;
        }
    }
    echo "Fetched All Statement Owner Payments" . PHP_EOL;
    return $ownerPayments;
}

function getAllSecurityDeposits($conn, array $bookings): array
{
    $securityDeposits = [];
    $sql = "
           SELECT * FROM security_deposits sd 
            JOIN booking b ON b.__pk = sd.bookingID
           WHERE b.to_date >= '" . FINANCIAL_YEAR_START_DATE . "' AND b._fk_property NOT IN (569930,545081,551573,487318,584020,540217,487426,573240,568437,487099,487107,540404,576515,585234,587312);";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (!empty($bookings[$obj->booking_id])) {
                $bookings[$obj->booking_id]->security_deposit_amount += $obj->depositamount;
            }
            $securityDeposits[$obj->booking_id][] = $obj;
        }
    }
    echo "Fetched All Bookings Security Deposits" . PHP_EOL;
    return $securityDeposits;
}

function getBookingChannelFee($booking): float
{
    $total = 0;
    foreach ($booking->extras as $extra) {
        if (stripos($extra->description, 'Character Cottages Channel Fee') !== false) {
            $total += $extra->total_price;
        }
    }

    return $total;
}

// Owner Cancellation/Compensation to CCH/ Reversal of Payments
function getManualOwnerAdjustments($conn, array $bookings): array
{
    $adjustments = [];
    $sql = " SELECT
              p.__pk,  
              opo.`owner_id`,
              opo.`payment_date`,
              opo.`paid_date`,
              opo.`payment_caption`,
              opo.`currency`,
              opo.`commission_amount`,
              opo.`commission_vat`,
              opo.`amount`
            FROM
              owner_payment_owe opo
              JOIN property p
                ON p.`_fk_owner` = opo.`owner_id`
            WHERE  opo.`booking_id` = 0 ";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            preg_match('(\d{6,8})', $obj->payment_caption, $matches);

            if (!empty($matches)) {
                $merged_temp_object = (object)array_merge(['booking_id' => $matches[0]], (array)$obj);
                $adjustments[$matches[0]][] = $merged_temp_object;
                if (!empty($booking[$matches[0]])){
                    $bookings[$matches[0]]->owner_payment_adjustments = $merged_temp_object;
                    $bookings[$matches[0]]->owner_payments_adjusted_amount += $obj->amount;
                }
            }
        }
    }
    echo "Fetched All Manual Owner Adjustments" . PHP_EOL;

    return $adjustments;
}

function getAmountPaidPerStatement($booking)
{
    $total = 0;

    foreach ($booking->owner_payments as $payment) {
        $total += $payment->amount;
    }

    foreach ($booking->owner_payment_adjustments as $payment) {
        $total += $payment->amount;
    }

    return $total;
}

function getFakeCash($booking)
{
    $total = 0;

    foreach ($booking->payments as $payment) {
        if (stripos(' ' . $payment->paymentcaption, 'ota') !== false) {
            $total += $payment->amount;
        }
    }

    return $total;
}

function getExtrasIncludedInRentalPrice($booking)
{
    $extrasTotal = 0;

    foreach ($booking->extras  as $item) {
        if ((int) $item->extra_type === 0) {
            $extrasTotal += $item->total_price;
        }
    }

    return $extrasTotal;
}
